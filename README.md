# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://Sumenjak@bitbucket.org/Sumenjak/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/Sumenjak/stroboskop/commits/d8b2bdffb4c70ff4adb189c4e068f53e9ba579f5

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/Sumenjak/stroboskop/commits/299157242d2f6379d8e94c1aed96c9ad98c5cf43

Naloga 6.3.2:
https://bitbucket.org/Sumenjak/stroboskop/commits/40bc5fc5753783ca9478f79f1d95e4571272ae59

Naloga 6.3.3:
https://bitbucket.org/Sumenjak/stroboskop/commits/b9f58af6603940989c0876bd3ca9a7ddd50fae08

Naloga 6.3.4:
https://bitbucket.org/Sumenjak/stroboskop/commits/ef66a275ab452967036d9bbb12420330e2a9be92

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/Sumenjak/stroboskop/commits/bbbf3814e88ae8cf41e12306afc7cf1dc7fec31e

Naloga 6.4.2:
https://bitbucket.org/Sumenjak/stroboskop/commits/a11a1bf80c2290600baf27b405a70a008a54cbaa

Naloga 6.4.3:
https://bitbucket.org/Sumenjak/stroboskop/commits/7109052f85a2b759825bff55ac2e4c25d87bb51a

Naloga 6.4.4:
https://bitbucket.org/Sumenjak/stroboskop/commits/8dc13bc777bc5af83db2d4430ca479e19eabbe66